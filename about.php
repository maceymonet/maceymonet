<?php include("header.inc.php"); ?>


  <div class=""  data-aos="fade">
    <div class="container-fluid">
      
      <div class="row justify-content-center">
        <div class="col-md-7">
          <div class="row mb-5 site-section">
            <div class="col-12 ">
              <h2 class="site-section-heading text-center">About Us</h2>
            </div>
          </div>

          <div class="row mb-5">
            <div class="col-md-7">
              <img src="images/mmmlogo.jpg" alt="Images" class="img-fluid">
            </div>
            <div class="col-md-4 ml-auto">
              <h3>Who is Macey Monet?</h3>
              <p>First impressions are made everyday. Less than 50 years ago, all first impressions were made in person or by word of mouth. Today, first impressions are made everyday on the internet. Today, how you look on Facebook, the beauty of your Instagram feed, and whether or not your website shows up first on Google search can have a large impact on your brand. Macey Monet Media is a branding company that wants to help you make a good first impression through digital media. </p>
            </div>
          </div>

         
          <div class="row site-section">
            <div class="col-md-6 col-lg-6 col-xl-4 text-center mb-5">
              <img src="images/haleyholty.jpg" alt="Image" class="img-fluid w-50 rounded-circle mb-4">
              <h2 class="text-black font-weight-light mb-4">Haley Holty</h2>
              <p class="mb-4">Head of production and founder of Macey Monet. Haley Holty is a digital media student from Western Technical College. Aside from running her own company, Haley runs her own personal youtube channel and likes to stay active by rock climbing and hiking with friends. She loves all things travel and trying new things. There's nothing she's afraid of... except heights.</p>
              <p>
                <a href="https://www.instagram.com/haleyholty/" class="pl-3 pr-3"><span class="icon-instagram"></span></a>
                <a href="https://www.youtube.com/haleyholty" class="pl-3 pr-3"><span class="icon-youtube-play"></span></a>
              </p>
            </div>
          </div>
        </div>
    
      </div>
    </div>
  </div>

  

<?php include("footer.inc.php"); ?>

    
    
  </div>

  <script src="js/jquery-3.3.1.min.js"></script>
  <script src="js/jquery-migrate-3.0.1.min.js"></script>
  <script src="js/jquery-ui.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.stellar.min.js"></script>
  <script src="js/jquery.countdown.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/bootstrap-datepicker.min.js"></script>
  <script src="js/swiper.min.js"></script>
  <script src="js/aos.js"></script>

  <script src="js/picturefill.min.js"></script>
  <script src="js/lightgallery-all.min.js"></script>
  <script src="js/jquery.mousewheel.min.js"></script>

  <script src="js/main.js"></script>
  
  <script>
    $(document).ready(function(){
      $('#lightgallery').lightGallery();
    });
  </script>
    
  </body>
</html>