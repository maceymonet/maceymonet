<?php
	if (isset($_POST["submit"])) {
		$firstName = $_POST['firstName'];
		$lastName = $_POST['lastName'];
		$email = $_POST['email'];
		$subject = $_POST['subject'];
		$message = $_POST['message'];
		
		$from = '@ MaceyMonet Contact Page @'; 
		$to = 'maceymonet@gmail.com'; 
		
		$body ="From: $firstName $lastName\nE-Mail: $email\nSubject: $subject\nMessage:\n$message";

		//VALIDATION
		/*
		if (!$_POST['firstName'] || !$_POST['lastName'] || !$_POST['email']
		|| !$_POST['subject'] || !$_POST['message'] ) {
			$error = 'Please fill all the required fields.';
		}
		
// If there are no errors, send the email
if (!$errName && !$errEmail && !$errMessage && !$errHuman) {
	*/
		if (mail ($to, $subject, $body, $from)) {
			$result='<div class="alert alert-success">Your email was successfully sent, thank you. I will be in touch!</div>';
		} else {
			$result='<div class="alert alert-danger">Sorry there was an error sending your message. Please try again later.</div>';
		}
	}
?>

<?php include("header.inc.php"); ?>
	
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>

<script type="text/javascript">

//RESET FORM
$( document ).ready(function() {
        $('#firstName').val('');
		$('#lastName').val('');
		$('#email').val('');
		$('#subject').val('');
		$('#message').val('');
    });

</script>
   

<?php 
	if(!isset($_POST['firstName'])) {
			$_POST['firstName'] = "";
			$_POST['lastName'] = "";
			$_POST['email'] = "";
			$_POST['subject'] = "";
			$_POST['message'] = "";
	}
	
	if(!isset($result)) {
			$result = "";
	}
	
	
?>




  <div class="site-section" data-aos="fade">
    <div class="container-fluid">
      
      <div class="row justify-content-center">
        <div class="col-md-7">
          <div class="row mb-5">
            <div class="col-12 ">
              <h2 class="site-section-heading text-center">Contact Us</h2>
            </div>
			
            <div class="col-12 ">
              <?php echo $result; ?>
            </div>
			

          </div>

          <div class="row">
            <div class="col-lg-8 mb-5">
			
			
			
              <form name="maceyForm" id="maceyForm" role="form" method="post" action="contact.php">
               

                <div class="row form-group">
                  <div class="col-md-6 mb-3 mb-md-0">
				  
					<label class="text-black" for="firstName">First Name</label>
					<input type="text" class="form-control" id="firstName" name="firstName" required="true" value="<?php echo htmlspecialchars($_POST['firstName']); ?>">
					
                  </div>
                  <div class="col-md-6">
				  
					<label class="text-black" for="lastName">Last Name</label>
					<input type="text" class="form-control" id="lastName" name="lastName" required="true" value="<?php echo htmlspecialchars($_POST['lastName']); ?>">
					
                  </div>
                </div>

                <div class="row form-group">
                  
                  <div class="col-md-12">
				  
					<label class="text-black" for="email">Email</label>
					<input type="email" class="form-control" id="email" name="email" required="true" value="<?php echo htmlspecialchars($_POST['email']); ?>">
					
                  </div>
                </div>

                <div class="row form-group">
                  
                  <div class="col-md-12">
					<label class="text-black" for="subject">Subject</label>
					<input type="subject" class="form-control" id="subject" name="subject" required="true" value="<?php echo htmlspecialchars($_POST['subject']); ?>">
                  </div>
                </div>

                <div class="row form-group">
                  <div class="col-md-12">
				  
					<label class="text-black" for="message">Message</label>
                    <textarea name="message" id="message" cols="30" rows="7" class="form-control" placeholder="Book a session, get a project quote, or send us your questions here..." required="true" value="<?php echo htmlspecialchars($_POST['message']); ?>"></textarea>
                  </div>
                </div>

                <div class="row form-group">
                  <div class="col-md-12">
                    <input type="submit" value="Submit" name="submit" class="btn btn-primary py-2 px-4 text-white"">
					
					<!--
                    <div class="btn-group">
                      <a class="btn btn-primary btn-email" href="mailto:maceymonet@gmail.com">Email Macey Monet</a>
                    </div>
					-->
                  </div>
                </div>
				


    
              </form>
			  
			  
			  
            </div>
            <div class="col-lg-3 ml-auto">
              <div class="mb-3 bg-white">
                <p class="mb-0"><a href="https://www.instagram.com/maceymonet/" class="pl-3 pr-3">Instagram <span class="icon-instagram"></span></a> </p>
                <p class="mb-0"><a href="mailto:maceymonet@gmail.com">maceymonet@gmail.com</a></p>

              </div>
              
            </div>
          </div>
        </div>
    
      </div>
    </div>
  </div>

  <?php include("footer.inc.php"); ?>

    

    
    
  </div>

  <script src="js/jquery-3.3.1.min.js"></script>
  <script src="js/jquery-migrate-3.0.1.min.js"></script>
  <script src="js/jquery-ui.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.stellar.min.js"></script>
  <script src="js/jquery.countdown.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/bootstrap-datepicker.min.js"></script>
  <script src="js/swiper.min.js"></script>
  <script src="js/aos.js"></script>

  <script src="js/picturefill.min.js"></script>
  <script src="js/lightgallery-all.min.js"></script>
  <script src="js/jquery.mousewheel.min.js"></script>

  <script src="js/main.js"></script>
  
  <script>
    $(document).ready(function(){
      $('#lightgallery').lightGallery();
    });
  </script>
    
  </body>
</html>