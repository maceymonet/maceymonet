<?php include("header.inc.php"); ?>



  <div class="site-section"  data-aos="fade">
    <div class="container-fluid">
      
      <div class="row justify-content-center">
        <div class="col-md-7">
          <div class="row mb-5">
            <div class="col-12 ">
              <h2 class="site-section-heading text-center">Our Services</h2>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6 col-lg-6 col-xl-4 text-center mb-5 mb-lg-5">
              <div class="h-100 p-4 p-lg-5 bg-light site-block-feature-7">
                <span class="icon flaticon-video-play display-3 text-primary mb-4 d-block"></span>
                <h3 class="text-black h4">Video Production</h3>
                <p>Have ideas for a video you just don't know how to make? Describe your project and let us bring it to life!</p>
                <p><strong class="font-weight-bold text-primary"><a href="https://forms.gle/YYUEX3PNnZopN2gQ9">Complete Our Project Form</a></strong></p>
              </div>
            </div>
            <div class="col-md-6 col-lg-6 col-xl-4 text-center mb-5 mb-lg-5">
              <div class="h-100 p-4 p-lg-5 bg-light site-block-feature-7">
                <span class="icon flaticon-camera display-3 text-primary mb-4 d-block"></span>
                <h3 class="text-black h4">Photography</h3>
                <p>Whether you need Senior Portaits, Family Photos, or Professional headshots, Macey Monet has what you need. Book a session today!</p>
                <p><strong class="font-weight-bold text-primary"><a href="mailto:maceymonet@gmail.com?subject=Photography Session For">Book a Session</a></strong></p>
              </div>
            </div>
            <div class="col-md-6 col-lg-6 col-xl-4 text-center mb-5 mb-lg-5">
              <div class="h-100 p-4 p-lg-5 bg-light site-block-feature-7">
                <span class="icon flaticon-frame display-3 text-primary mb-4 d-block"></span>
                <h3 class="text-black h4">Graphic Design</h3>
                <p>Contact us to check out our branding packages which could include: Logo Design, Posters, Business Cards, Social Media Banners, and more!</p>
                <p><strong class="font-weight-bold text-primary"><a href="mailto:maceymonet@gmail.com?subject=What Are Your Logo Design Packages?">Get A Quote</a></strong></p>
              </div>
            </div>
            <div class="col-md-6 col-lg-6 col-xl-4 text-center mb-5 mb-lg-5">
              <div class="h-100 p-4 p-lg-5 bg-light site-block-feature-7">
                <span class="icon flaticon-video-play display-3 text-primary mb-4 d-block"></span>
                <h3 class="text-black h4">Web Design</h3>
                <p>Need a Website or need someone to manage your website? Hire our services to keep your website looking tasty.</p>
                <p><strong class="font-weight-bold text-primary"><a href="mailto:maceymonet@gmail.com?subject=Web Design Query">Get a Trial Run!</a></strong></p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

<?php include("footer.inc.php"); ?>

    

    
    
  </div>

  <script src="js/jquery-3.3.1.min.js"></script>
  <script src="js/jquery-migrate-3.0.1.min.js"></script>
  <script src="js/jquery-ui.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.stellar.min.js"></script>
  <script src="js/jquery.countdown.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/bootstrap-datepicker.min.js"></script>
  <script src="js/swiper.min.js"></script>
  <script src="js/aos.js"></script>

  <script src="js/picturefill.min.js"></script>
  <script src="js/lightgallery-all.min.js"></script>
  <script src="js/jquery.mousewheel.min.js"></script>

  <script src="js/main.js"></script>
  
  <script>
    $(document).ready(function(){
      $('#lightgallery').lightGallery();
    });
  </script>
    
  </body>
</html>